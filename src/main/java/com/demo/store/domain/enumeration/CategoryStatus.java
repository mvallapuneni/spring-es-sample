package com.demo.store.domain.enumeration;

/**
 * The CategoryStatus enumeration.
 */
public enum CategoryStatus {
    AVAILABLE,
    RESTRICTED,
    DISABLED,
}
