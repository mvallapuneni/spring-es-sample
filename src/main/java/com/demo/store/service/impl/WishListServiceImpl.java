package com.demo.store.service.impl;

import com.demo.store.domain.WishList;
import com.demo.store.repository.WishListRepository;
import com.demo.store.repository.search.WishListSearchRepository;
import com.demo.store.service.WishListService;
import com.demo.store.service.dto.WishListDTO;
import com.demo.store.service.mapper.WishListMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link WishList}.
 */
@Service
@Transactional
public class WishListServiceImpl implements WishListService {

    private final Logger log = LoggerFactory.getLogger(WishListServiceImpl.class);

    private final WishListRepository wishListRepository;

    private final WishListMapper wishListMapper;

    private final WishListSearchRepository wishListSearchRepository;

    public WishListServiceImpl(
        WishListRepository wishListRepository,
        WishListMapper wishListMapper,
        WishListSearchRepository wishListSearchRepository
    ) {
        this.wishListRepository = wishListRepository;
        this.wishListMapper = wishListMapper;
        this.wishListSearchRepository = wishListSearchRepository;
    }

    @Override
    public WishListDTO save(WishListDTO wishListDTO) {
        log.debug("Request to save WishList : {}", wishListDTO);
        WishList wishList = wishListMapper.toEntity(wishListDTO);
        wishList = wishListRepository.save(wishList);
        WishListDTO result = wishListMapper.toDto(wishList);
        wishListSearchRepository.save(wishList);
        return result;
    }

    @Override
    public Optional<WishListDTO> partialUpdate(WishListDTO wishListDTO) {
        log.debug("Request to partially update WishList : {}", wishListDTO);

        return wishListRepository
            .findById(wishListDTO.getId())
            .map(existingWishList -> {
                wishListMapper.partialUpdate(existingWishList, wishListDTO);

                return existingWishList;
            })
            .map(wishListRepository::save)
            .map(savedWishList -> {
                wishListSearchRepository.save(savedWishList);

                return savedWishList;
            })
            .map(wishListMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WishListDTO> findAll() {
        log.debug("Request to get all WishLists");
        return wishListRepository.findAll().stream().map(wishListMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<WishListDTO> findOne(Long id) {
        log.debug("Request to get WishList : {}", id);
        return wishListRepository.findById(id).map(wishListMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete WishList : {}", id);
        wishListRepository.deleteById(id);
        wishListSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WishListDTO> search(String query) {
        log.debug("Request to search WishLists for query {}", query);
        return StreamSupport
            .stream(wishListSearchRepository.search(query).spliterator(), false)
            .map(wishListMapper::toDto)
            .collect(Collectors.toList());
    }
}
