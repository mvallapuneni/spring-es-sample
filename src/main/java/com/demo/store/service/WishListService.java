package com.demo.store.service;

import com.demo.store.service.dto.WishListDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.demo.store.domain.WishList}.
 */
public interface WishListService {
    /**
     * Save a wishList.
     *
     * @param wishListDTO the entity to save.
     * @return the persisted entity.
     */
    WishListDTO save(WishListDTO wishListDTO);

    /**
     * Partially updates a wishList.
     *
     * @param wishListDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<WishListDTO> partialUpdate(WishListDTO wishListDTO);

    /**
     * Get all the wishLists.
     *
     * @return the list of entities.
     */
    List<WishListDTO> findAll();

    /**
     * Get the "id" wishList.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WishListDTO> findOne(Long id);

    /**
     * Delete the "id" wishList.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the wishList corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    List<WishListDTO> search(String query);
}
