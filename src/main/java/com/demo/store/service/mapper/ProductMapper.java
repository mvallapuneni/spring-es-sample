package com.demo.store.service.mapper;

import com.demo.store.domain.Product;
import com.demo.store.service.dto.ProductDTO;
import java.util.Set;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = { WishListMapper.class })
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
    @Mapping(target = "wishList", source = "wishList", qualifiedByName = "id")
    ProductDTO toDto(Product s);

    @Named("titleSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "title", source = "title")
    Set<ProductDTO> toDtoTitleSet(Set<Product> product);
}
