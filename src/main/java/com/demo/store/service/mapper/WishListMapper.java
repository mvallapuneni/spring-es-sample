package com.demo.store.service.mapper;

import com.demo.store.domain.WishList;
import com.demo.store.service.dto.WishListDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link WishList} and its DTO {@link WishListDTO}.
 */
@Mapper(componentModel = "spring", uses = { CustomerMapper.class })
public interface WishListMapper extends EntityMapper<WishListDTO, WishList> {
    @Mapping(target = "customer", source = "customer", qualifiedByName = "id")
    WishListDTO toDto(WishList s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    WishListDTO toDtoId(WishList wishList);
}
